<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Welcome || Blog</title>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('images/favicons/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('images/favicons/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('images/favicons/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('images/favicons/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('images/favicons/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('images/favicons/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('images/favicons/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('images/favicons/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('images/favicons/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('images/favicons/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('images/favicons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('images/favicons/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/favicons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('images/favicons/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('images/favicons/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">

    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700%7CLibre+Baskerville:400,400italic,700'
          rel='stylesheet'
          type='text/css'>
    <!-- Styles -->
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main-page-style.css') }}" rel="stylesheet">
</head>
<body class="home blog">
    <!-- Preloader Gif -->
    <table class="doc-loader">
        <tbody>
        <tr>
            <td>
                <img src="{{asset('images/ajax-document-loader.gif')}}" alt="Loading...">
            </td>
        </tr>
        </tbody>
    </table>

    <!-- Left Sidebar -->
    <div id="sidebar" class="sidebar">
        <div class="menu-left-part">
            <div class="search-holder">
                <label>
                    <input type="search" class="search-field" placeholder="Type here to search..." value="" name="s" title="Search for:">
                </label>
            </div>
            <div class="site-info-holder">
                <h1 class="site-title">Blog.</h1>
                <p class="site-description">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </p>
            </div>
            <nav id="header-main-menu">
                <ul class="main-menu sm sm-clean">
                    <li><a href="{{url('/')}}" class="current">Home</a></li>
                    <li><a href="about.html">About</a></li>
                    <li><a href="scroll.html">Scroll</a></li>
                    <li><a href="contact.html">Contact</a></li>
                </ul>
            </nav>
            <footer>
                <div class="footer-info">
                    © 2018 BLOG. <br> CRAFTED WITH <i class="fa fa-heart"></i> BY <a href="javascript:void(0)">GT</a>.
                </div>
            </footer>
        </div>
        <div class="menu-right-part">
            <div class="logo-holder">
                <a href="{{url('/')}}">
                    <img src="{{asset('images/logo.png')}}" alt="Blog Logo">
                </a>
            </div>
            <div class="toggle-holder">
                <div id="toggle">
                    <div class="menu-line"></div>
                </div>
            </div>
            <div class="social-holder">
                <div class="social-list">
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-youtube-play"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-vimeo"></i></a>
                    <a href="#"><i class="fa fa-behance"></i></a>
                    <a href="#"><i class="fa fa-rss"></i></a>
                </div>
            </div>
            <div class="fixed scroll-top"><i class="fa fa-caret-square-o-up" aria-hidden="true"></i></div>
        </div>
        <div class="clear"></div>
    </div>

    <div id="app">
        @yield('content')
    </div>

    <!-- Scripts -->
    <!--<script src="{{ asset('js/all.js') }}"></script>-->
    <!--Load JavaScript-->
    <script type="text/javascript" src="{{asset('js/libraries/jquery.js')}}"></script>
    <script type='text/javascript' src="{{asset('js/libraries/imagesloaded.pkgd.js')}}"></script>
    <script type='text/javascript' src="{{asset('js/libraries/jquery.nicescroll.min.js')}}"></script>
    <script type='text/javascript' src="{{asset('js/libraries/jquery.smartmenus.min.js')}}"></script>
    <script type='text/javascript' src="{{asset('js/libraries/jquery.carouFredSel-6.0.0-packed.js')}}"></script>
    <script type='text/javascript' src="{{asset('js/libraries/jquery.mousewheel.min.js')}}"></script>
    <script type='text/javascript' src="{{asset('js/libraries/jquery.touchSwipe.min.js')}}"></script>
    <script type='text/javascript' src="{{asset('js/libraries/jquery.easing.1.3.js')}}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
