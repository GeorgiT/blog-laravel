@extends('layouts.admin')

@section('title', 'User List.')

@section('content')
<div data-notify="container" class="col-xs-11 col-sm-4 alert alert-success alert-with-icon animated fadeInDown hidden" role="alert" data-notify-position="top-center" style="display: inline-block; margin: 0 auto; position: fixed; transition: all 0.5s ease-in-out; z-index: 1031; top: 20px; left: 0; right: 0;">
    <button type="button" aria-hidden="true" class="close" data-notify="dismiss" style="position: absolute; right: 10px; top: 50%; margin-top: -13px; z-index: 1033;">×</button>
    <i data-notify="icon" class="material-icons">notifications</i>
    <span data-notify="title"></span>
    <span data-notify="message"></span>
    <a href="#" target="_blank" data-notify="url"></a>
</div>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">User List</h4>
                        <p class="category">Here is a list of all users</p>
                    </div>
                    <div class="card-content table-responsive">
                        <table class="table">
                            @if( $role == 'owner' )
                            <thead class="text-primary">
                            <th>Name</th>
                            <th>Mail</th>
                            <th>Join Date</th>
                            <th>Role</th>
                            </thead>
                            <tbody>
                            @foreach ($selects as $select)
                                <tr>
                                    <td>{{ $select->name }}</td>
                                    <td>{{ $select->email }}</td>
                                    <td class="text-primary">{{ $select->created_at }}</td>
                                    <td>
                                        <select class="form-control form-info no-margin change-role" data-user="{{ $select->role }}" data-id="{{ $select->id }}">
                                            <option value="owner">Owner</option>
                                            <option value="admin">Admin</option>
                                            <option value="user">User</option>
                                        </select>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            @elseif( $role == 'admin' )
                            <thead class="text-primary">
                            <th>Name</th>
                            <th>Mail</th>
                            <th>Join Date</th>
                            <th>Role</th>
                            </thead>
                            <tbody>
                            @foreach ($selects as $select)
                            <tr>
                                <td>{{ $select->name }}</td>
                                <td>{{ $select->email }}</td>
                                <td class="text-primary">{{ $select->created_at }}</td>
                                <td>{{ $select->role }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                            @endif

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
