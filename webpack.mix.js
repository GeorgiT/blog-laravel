let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.copy('resources/assets/css/main-page-style.css', 'public/css');


// Main page
mix.styles([
    'resources/assets/css/libraries/clear.css',
    'resources/assets/css/libraries/common.css',
    'resources/assets/css/libraries/font-awesome.min.css',
    'resources/assets/css/libraries/carouFredSel.css',
    'resources/assets/css/libraries/sm-clean.css'
], 'public/css/all.css');

mix.js([
    'resources/assets/js/libraries/jquery.js',
    'resources/assets/js/libraries/imagesloaded.pkgd.js',
    'resources/assets/js/libraries/jquery.nicescroll.min.js',
    'resources/assets/js/libraries/jquery.smartmenus.min.js',
    'resources/assets/js/libraries/jquery.carouFredSel-6.0.0-packed.js',
    'resources/assets/js/libraries/jquery.mousewheel.min.js',
    'resources/assets/js/libraries/jquery.touchSwipe.min.js',
    'resources/assets/js/libraries/jquery.easing.1.3.js'
], 'public/js/all.js');

// Admin page
mix.styles([
    'resources/assets/css/admin-libraries/bootstrap.min.css',
    'resources/assets/css/admin-libraries/material-dashboard.css'
], 'public/css/admin-all.css');
