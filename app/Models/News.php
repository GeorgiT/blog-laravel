<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
        'eng_title', 'arm_title', 'eng_description', 'arm_description', 'category'
    ];

}

