<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ChangeRoleController extends Controller
{
    public function index(Request $request){
        $id = $request->user_id;
        $new_role = $request->role;
        User::where('id', $id)
            ->update(['role' => $new_role]);
        return response()->json(['status'=>'Role changed successfully']);
    }
}
