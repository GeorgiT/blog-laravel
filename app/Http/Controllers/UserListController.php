<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;


class UserListController extends Controller
{

    public function role()
    {
        $user = auth()->user();
        $id = $user->id;
        $role = $user->role;
        if($role == 'owner'){
            $selects = User::all()->where('id', '!=', $id);
            return view('admin.user-list', compact('selects','role'));
        }else if($role == 'admin'){
            $selects = User::all()->where('id', '!=', $id);
            return view('admin.user-list', compact('selects', 'role'));
        }else if($role == 'user'){
            return redirect('home')->with('status', 'Only Owner and Admin can see this page!');
        }
    }
}
